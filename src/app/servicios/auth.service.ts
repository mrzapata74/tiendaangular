import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http, Response } from '@angular/http';

//Importando clase Producto para la funcion InsertarProducto()
import { producto } from '../modelo/producto';

/* Se importa AngularFireDatabase para usar la conexion a la firebase y 
AngularFireList para poder crear listas con los datos obtenidos */
import { AngularFireDatabase, AngularFireList } from  'angularfire2/database'
import { promise } from 'protractor';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';

import * as firebase from 'firebase/app';
import 'firebase/database';

import { environment } from '../../environments/environment';


@Injectable({providedIn:'root'})

export class AuthService {

  private url:string;
   todosProd: any;
   carritoLista: any[] = [];
   subtotalLista: any[] = [];

  selectedProducto: producto = new producto();

  constructor(public router: Router, public afAuth : AngularFireAuth, //Vinculamos la variable afAuth al modulo AngularFireAuth, 
                                      // para controlar autenticacion y validacion de usuarios.
    // private firebase: AngularFireDatabase, //Vinculamos la variable firebase al modulo importado para firebase
    private http: HttpClient,
    private http2: Http
    // private dbFirebase: AngularFireDatabase
    ) { 
/*       var config = {
        apiKey: "AIzaSyBWUFghVz9Qsug5S_RsFbL_n0q_AoN6Pkk",
        authDomain: "tiendaangular-d9d57.firebaseapp.com",
        databaseURL: "https://tiendaangular-d9d57.firebaseio.com",
        projectId: "tiendaangular-d9d57",
        storageBucket: "tiendaangular-d9d57.appspot.com",
        messagingSenderId: "669503873995"
      };

      firebase.initializeApp(config);
      var db =  firebase.database(); */

      console.log("carritoLista ver "+this.subtotalLista)
      
      
      this.capturarProductos();

     
    }
    
 /*    actualiza(id: any, datosNuevos: any){
      this.dbFirebase.list('productos').update(id, datosNuevos);
    } */

    //Comprobar Usuario Logueado
    usuarioLogin(){
      return this.afAuth.authState.map(auth => auth);
    }

     //Obtener Productos
/*   obtenerProductos()
  {
    return this.listaProductos = this.firebase.list('productos');

  } */




  
  //4 ENERO 2019
  getProductos(){
    return this.http.get('https://tiendaangular-d9d57.firebaseio.com/productos/.json');
  }

  //4 DE ENERO 2019
  capturarProductos(){
    this.getProductos().subscribe((data)=>this.todosProd = data)
  }


  //6 DE ENERO 2019
  agregaProductoNuevo(addProducto: producto){
    console.log("PRODUCTOS QUE LLEGAN AL SERVICIO "+JSON.stringify(addProducto))
    this.todosProd.push(addProducto)
    firebase.database().ref('/productos/').set(this.todosProd)
  }






/*  ----------------------------------- */

//PRUEBA DE OTRA FORMA DE OBTENCION DE PRODUCTOS

  /* Funcion que trae todos los productos de la BD de firebase */
  getTodosProd(){
    const urlProd = 'https://tiendaangular-d9d57.firebaseio.com/productos/.json';
    return this.http.get(urlProd);
  }

 /*  Funcion que consulta un producto a firebase a traves de su id, que viene desde la funcion
  obtenerDetalles pasada por parametro. */
  getUnProducto(id: string){
    const urlProdOne = `https://tiendaangular-d9d57.firebaseio.com/productos/${id}/.json`;
    console.log("RESPUESTA DE UN PRODUCTO "+JSON.stringify(urlProdOne))
    return this.http.get(urlProdOne);
    
  }



  /*  ----------------------------------- */


  //Insertar Producto a la BD utilizando la clase producto de la carpeta Modelo
/*     insertarProducto(addProducto: producto){
      this.listaProductos.push({
        nombreProd : addProducto.nomProd,
        precioProd : addProducto.precioProd,
        existenciaProd: addProducto.existenciaProd,
        // imagenProd: addProducto.imagenProd 
      })
    } */

    //Actualizar Producto de la BD
    actualizarProducto(updateProducto: any){
     let idP = updateProducto.index;
    let ddd = `https://tiendaangular-d9d57.firebaseio.com/productos/${idP}`
    console.log("index que llega ", ddd)
     this.todosProd.update(ddd,{updateProducto
 /*       existenciaProd: updateProducto.existenciaProd,
       idProd: updateProducto.idProd,
       imgProd: updateProducto.imgProd,
       nombreProd: updateProducto.nombreProd,
       precioProd: updateProducto.precioProd */
     })
     firebase.database().ref("/productos/").set(this.todosProd)
    } 

    //Eliminiar Producto de la BD
/*     eliminarProducto($key: string){
      this.listaProductos.remove($key);
    } */


 
  //Cerrando Sesion
  cierraSesion(){
    return this.afAuth.auth.signOut();
 
  }
  
  //Registro de Usuario Nuevo
  nuevoUsuario(email: string, pass: string) {
    return new Promise((resolve, reject)=> {
      this.afAuth.auth.createUserWithEmailAndPassword(email, pass)
      .then(userData => resolve(userData),
      err => reject (err));
    });
  }

  //Login de Usuario
  ingresoUsuario(email: string, pass: string) {
    return new Promise((resolve, reject)=> {
      this.afAuth.auth.signInWithEmailAndPassword(email, pass)
      .then(userData => resolve(userData),
      err => reject (err));
    });
  }

  

  //Observador de usuario logueado
  obserAuth(){
     return this.afAuth.authState.map( auth => auth);
/*     firebase.auth().onAuthStateChanged(function(user){
      var user = firebase.auth().currentUser;
      if(user){
        window.location.href='navbar.html';
      }else{
        console.log("No existe ese usuario");
      }
    }) */
  }

  //Datos que vienen del componente Catalogo.ts
   public datosCardProducto(data: any){
   
     this.carritoLista.push(data);
     console.log("Auth "+ JSON.stringify(data));
     

     this.subtotalLista.push(data.subtotal);
     console.log("Subtotales "+ JSON.stringify(data.subtotal));

    }

    //Retorna la cantidad de items del carrito
    contarCarrito(){
      return this.carritoLista.length
    }






  
}
