import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../servicios/auth.service';
import { ActivatedRoute, Params} from '@angular/router';



@Component({
  selector: 'app-detproducto',
  templateUrl: './detproducto.component.html',
  styleUrls: ['./detproducto.component.css']
})
export class DetproductoComponent implements OnInit {
  
  /* Inicializada como any, para que reciba cualquier tipo de dato de la BD */
  detProd: any = [];

  
  constructor(
    private servicio:AuthService,
    private route: ActivatedRoute

  ) {  }

/*     private detProducto :  producto = {
      $key:'',
      imagenProd:'',
      nomProd:'',
      precioProd:'',
      existenciaProd:''
    } */
    


  ngOnInit() {
   /*  idProd captura el id de producto que viene en el URL desde Catalogo, al hacer
    clic en "Ver Mas", luego se le pasa a la funcion this.obtenerDetalles */
    const id = this.route.snapshot.params['id'];
    this.obtenerDetalles(id);
 

    // console.log("DETALLEES PRODUCTOS "+this.detProd)
 
  }

 /*  Funcion que invoca en auth.service.ts, la funcion getUnProducto, que trae
  por id el producto del id que se le envia en el parametro obtenerDetalles. */
  obtenerDetalles(id: string){
    this.servicio.getUnProducto(id).subscribe(datos => (this.detProd = datos));
    console.log("DATOS DETPROD EN FUNCION OBTENER DETALLES "+JSON.stringify(this.detProd))
    // console.log(id);
  }



}
