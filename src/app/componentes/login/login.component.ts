import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../servicios/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public email: string;
  public password: string;

  constructor(
    //Definiendo variable conectada al servicio
    public servicio: AuthService,
    //Definiendo variable router conectada al Router
    public router: Router
  ) { }

  ngOnInit() {
  }

  onSubmitLogin(){
    this.servicio.ingresoUsuario(this.email, this.password)
    .then( (res) =>{
      console.log('Usuario Correcto')
      this.router.navigate(['home']);
    }).catch( (err) =>{
      console.log('Usuario No Encontrado')
      this.router.navigate([''])
    })
  }



}
