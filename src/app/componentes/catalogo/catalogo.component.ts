import { Component, OnInit } from '@angular/core';
import { reject } from 'q';
import { AuthService } from '../../servicios/auth.service';
import { producto } from '../../modelo/producto';



@Component({
  selector: 'app-catalogo',
  templateUrl: './catalogo.component.html',
  styleUrls: ['./catalogo.component.css']
})
export class CatalogoComponent implements OnInit {
  filtroPrecio: string = '';
  filtro: string = '';
  listaProductos: any;

  // prodcomprado: any[];

  constructor(private servicio: AuthService) { }

  ngOnInit() {

    this.verProductos();

  }//AQUI FINALIZA EL NGONINIT


  

  verProductos(){
    return this.servicio.getProductos().subscribe((datos)=>this.listaProductos = datos)
  }


    /* Funcion que recibe los datos enviados desde el card producto */
  comprar(imgprod: string, id:string, item: string, p: number, v: number){
    let img: string = imgprod;
    let idProd: string = id;
    let cantidad: number = v;
    let nombre: string = item;
    let precio: number = p;
    let subtotal = cantidad * precio;

    let datos: any;

     datos = {img, idProd, nombre, cantidad, precio, subtotal};
    // console.log("Array del catalogo TS "+ JSON.stringify(datos) );
    this.servicio.datosCardProducto(datos);


  }



}
