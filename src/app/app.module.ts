import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";
import { HttpModule } from '@angular/http';

/* Importando elemento para controlar formularios [(ngModule)] por ejemplo*/
import { FormsModule } from '@angular/forms';

/* Importando componente principal */
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

/* Importando Servicios */
import { AuthService } from './servicios/auth.service';

/* Importando Pipe */
import { NgPipesModule } from 'ngx-pipes';

/* Importando Modulos para conectarse a Firebase */
import { AngularFireModule } from 'angularfire2';//Esencial para Iniciar la conexion del environment
import { AngularFireAuthModule } from 'angularfire2/auth';//Esencial para Autenticar Usuarios
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from '../environments/environment';//Contiene las Credenciales de la API de nuestro proyecto

/* Importando storage para almacenar imagenes */
import { AngularFireStorageModule } from '@angular/fire/storage';

/* Importando Componentes */
import { CatalogoComponent } from './componentes/catalogo/catalogo.component';
import { LoginComponent } from './componentes/login/login.component';
import { NavbarComponent } from './componentes/navbar/navbar.component';
import { CarritoComponent } from './componentes/carrito/carrito.component';
import { DetproductoComponent } from './componentes/detproducto/detproducto.component';
import { ContactoComponent } from './componentes/contacto/contacto.component';
import { RegistroComponent } from './componentes/registro/registro.component';
import { HomeComponent } from './componentes/home/home.component';
import { ProductoComponent } from './componentes/producto/producto.component';
import { EditprodComponent } from './componentes/editprod/editprod.component';

import { SidebarComponent } from './componentes/sidebar/sidebar.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    RegistroComponent,
    CatalogoComponent,
    CarritoComponent,
    DetproductoComponent,
    ContactoComponent,
    HomeComponent,
    ProductoComponent,
    EditprodComponent,
    SidebarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgPipesModule,
    HttpClientModule,
    HttpModule,
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(environment.firebase),//se conecta a environment.ts para conectarse a firebase
    AngularFireStorageModule

  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
