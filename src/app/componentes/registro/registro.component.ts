import { Component, OnInit } from '@angular/core';
import { AuthService } from "../../servicios/auth.service";
import { AppRoutingModule } from "../../app-routing.module";

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {
  public email : string;
  public password : string;
  constructor(
    public servicio: AuthService
  ) { }

  ngOnInit() {
  }


  onSubmitAddUser(){
    this.servicio.nuevoUsuario(this.email, this.password)
    .then( (res) => {
      console.log("Agregado Correctamente");
      console.log(res);
      window.setTimeout(function(){
        window.location.href= '/';
      }, 1000);
     }).catch( (err) => {
      console.log(err);
      console.log("!ERROR AL INGRESAR NUEVO USUARIO")
    })
  }


}
