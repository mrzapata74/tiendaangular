import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../servicios/auth.service';
import { NgForm } from '@angular/forms';//para usar el NgForm de la funcion resetForm()
import { producto } from 'src/app/modelo/producto';
import { AngularFireStorage } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';//Para recuperar la url del storage
import { Observable } from 'rxjs';
import { $ } from 'protractor';

import * as firebase from 'firebase/app';
import 'firebase/database';
import { snapshotChanges } from 'angularfire2/database';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {

  subidaPorcentaje: Observable<number>;//Para observar el porcentaje de subida de la imagen
  urlImagenSubida: any;



  // private imgP: any = 'Mario';

  archSelecc:any;
  uploadValue:number;
  urlImagen:string;
  bytestransferred:number

  constructor(
    private authService: AuthService,
    private storage: AngularFireStorage
    
  ) { }    


  ngOnInit() {


    // console.log("URL SUBIDA EN NGONINIT "+this.urlImagenSubida)

  }

  public imgSeleccionada(event){
    console.log('NDatos', event.target.files[0])
    document.getElementById("imagePreview").style.backgroundImage = `url('../../../assets/${event.target.files[0].name}')`;
    this.archSelecc = event.target.files[0];
    console.log("EN VARIABLE ARCHSELECC ", this.archSelecc)
    
    this.onTrepar();
  }


  onTrepar(){
    var filename = this.archSelecc.name;
    var storageRef = firebase.storage().ref('/imgProductos/'+filename);
    var uploadTask = storageRef.put(this.archSelecc);
    
    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, function(snapshot){

    }, function(error){

    }, function(){

      uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL){  
        // this.urlImagenSubida === downloadURL;
        document.getElementById('imgProd').setAttribute('value', downloadURL);
        console.log('URL OBTENIDO ', downloadURL);
        // console.log('contenido de urlImagenSubida'+this.urlImagenSubida)
        //  this.urlImagenSubida = 'Mario Zapata';
      
   
      })

      // var downloadLink = uploadTask.snapshot.downloadURL;
 

    })

  }



/*   upload(){
    const storageRef = firebase.storage().ref(`/imgProductos/${this.archSelecc.name}`);
    const tarea = storageRef.put(this.archSelecc);
    tarea.on('cambio_estado', snapshot=>{
      let bytestransferred = null;
      let porc = (snapshot.bytestransferred /snapshot.totalBytes)*100;
      this.uploadValue = porc;

    }, error=>{console.log(error.message)},
    ()=>{this.uploadValue=100;
      tarea.snapshot.ref.getDownloadURL().then((url)=>{
        this.urlImagenSubida = url;
        console.log('EN VARIABLE URLIMAGEN', this.urlImagen);
      });  
    });
  }
 */
  //Funcion que carga la imagen al html y guarda dicha imagen en storage para obtener la ruta de almacenamiento de la imagen
/*   onSubir(e:any){

    //Imprimiendo el objeto de imagen que llega a la funcion
    console.log('Imagen', e.target.files[0]);
    //Carga la imagen que se elige en el html
     document.getElementById("imagePreview").style.backgroundImage = `url('../../../assets/${e.target.files[0].name}')`;

    //Generar llave aleatoria para la imagen, para que no se sobreescriba al imagen si se sube un nombre igual
    const id = Math.random().toString(36).substring(2);
    const file = e.target.files[0];
    const ruta = `imgProductos/${id}`;
    const ref = this.storage.ref(ruta);
    const tareaSubir = this.storage.upload(ruta, file); 

    // this.subidaPorcentaje = tareaSubir.percentageChanges();
 
    //Recuperarndo la URL del la imagen almacenada
    tareaSubir.snapshotChanges().pipe(finalize(()=>this.urlImagenSubida = ref.getDownloadURL())).subscribe();
      console.log("URL SUBIDA EN FUNCION "+JSON.stringify(ref.getDownloadURL()))

    

  } */

/*   guardarLocalStorage(){
    let datoUrl = this.urlImagenSubida;

    localStorage.setItem('URL', datoUrl);
  }
 */

 enviaProd(imgP: string, idP: number, nomP: string, precP: number, exP: number){
    let imgProd : string = imgP;
    let idProd : number = idP;
    let nombreProd : string = nomP;
    let precioProd : number = precP;
    let existenciaProd : number = exP;

    let dataProd : any;

    dataProd = {imgProd, idProd, nombreProd, precioProd, existenciaProd};

    this.authService.agregaProductoNuevo(dataProd);


    // console.log("DATOS QUE LLEGAN AL TS producto "+JSON.stringify(dataProd))

 }


  //Funcion que resetea el formulario productos, 
  //pero tambien la variable que aloja temporalmente los datos que muestra el formulario
  resetForm(form){
    
    document.getElementById('imgProd').setAttribute('value', "");
    document.getElementById("imagePreview").style.backgroundImage = `url('')`;
 

  }
  
/*   resetForm(productoForm?: NgForm){
    if(productoForm != null){
      productoForm.reset();
      this.authService.selectedProducto = new producto//Esta linea deja la variable selectedProducto en blanco
    }
  } */

}
