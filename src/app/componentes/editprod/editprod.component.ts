import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../servicios/auth.service';


import * as firebase from 'firebase/app';
import 'firebase/database';


@Component({
  selector: 'app-editprod',
  templateUrl: './editprod.component.html',
  styleUrls: ['./editprod.component.css']
})
export class EditprodComponent implements OnInit {
filtro: string = '';
  listaProductos: any;
  imgSelect: any;

  constructor(private servicio: AuthService) { }

  ngOnInit() {
    this.verProductos();
  }

  verProductos(){
    return this.servicio.getProductos().subscribe((datos)=>this.listaProductos = datos)
  }


  editarPcto(pos: string, img: string, id:string, nombre:string, precio:string, exist:string){
    document.getElementById('pos').setAttribute('value', pos);
    document.getElementById('idProd').setAttribute('value', id);
    document.getElementById('nombreProd').setAttribute('value', nombre);
    document.getElementById('precioProd').setAttribute('value', precio);
    document.getElementById('existProd').setAttribute('value', exist);
  } /* Aqui Termina funcion editarPcto */


  public imgCambio(event){
    // console.log('NDatos', event.target.files[0])
    document.getElementById("imagePreview").style.backgroundImage = `url('../../../assets/${event.target.files[0].name}')`;
    this.imgSelect = event.target.files[0];
    console.log("EN VARIABLE imgSelect ", this.imgSelect)
    
     this.onSubeImg();
  } /* Aqui Termina funcion imgCambio */

  onSubeImg(){
    var filename = this.imgSelect.name;
    var almacRef = firebase.storage().ref('/imgProductos/'+filename);
    var uploadTask = almacRef.put(this.imgSelect);

    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, function(snapshot){

    }, function(error){

    }, function(){

      uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL){  
        document.getElementById('imgProd').setAttribute('value', downloadURL);
        console.log('URL OBTENIDO ', downloadURL);   
   
      })
    })
  } /* Aqui Termina funcion onSubeImg */


 enviaProdEdit(i: number, imgP: string, idP: number, nomP: string, precP: number, exP: number){
    let index : number = i;
    let imgProd : string = imgP;
    let idProd : number = idP;
    let nombreProd : string = nomP;
    let precioProd : number = precP;
    let existenciaProd : number = exP;
    
    let dataEdit : any;

    dataEdit = {index, imgProd, idProd, nombreProd, precioProd, existenciaProd};
    console.log(JSON.stringify(dataEdit));
    this.servicio.actualizarProducto(dataEdit);
  
  } 



}/* Aqui Termina la clase  EditprodComponent */
