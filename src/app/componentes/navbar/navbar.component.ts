import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../servicios/auth.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {



    verItem: any[]

  constructor(
    public router:Router,
    public servicio: AuthService


    
  ) {
/*     this.verItem = this.servicio.itemCarrito; */
    // console.log("En el NavBar "+this.verItem.length);

  }

  ngOnInit() {


  }


  compruebaLogin(){
    this.servicio.usuarioLogin().subscribe(auth => {
      if(auth){
        console.log(auth);
      }
    })
  }

  salir(){
    this.servicio.cierraSesion();
    this.router.navigate(['']);
    console.log('Session cerrada');
  }


}
