import { Component, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';
import { AuthService } from '../../servicios/auth.service';
import { producto } from '../../modelo/producto';
import { Router } from '@angular/router';
import { AngularFireDatabase, AngularFireList } from  'angularfire2/database';
import { environment } from '../../../environments/environment';

import * as firebase from 'firebase/app';
import 'firebase/database';


@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.css']
}) 

export class CarritoComponent implements OnInit {

  listaProd: any[];//Para actualizar cantidad

  carrito: any = [];
  total: any = [];
  tItemCarr: any = [];

  constructor(private servicio:AuthService, private router: Router, private http: Http, private afire:AngularFireDatabase) {

     this.carrito = this.servicio.carritoLista;//variable carrito se llena con el array de carritolista en el servicio
     this.total = this.servicio.subtotalLista;//variable total se llena con el array de subtotalLista en el servicio
    this.listaProd = this.servicio.todosProd;
    //  this.servicio.itemCar(this.tItemCarr);
    
   }

  ngOnInit() {


console.log("ListaProd desde el Carrito "+JSON.stringify(this.listaProd));
console.log("ListaCarrito"+ JSON.stringify( this.carrito));

  }

  //Funcion que actualiza las existencias en la BD en Firebase
  pagar(){
    for(let itemProd in this.listaProd){
      // console.log("solo lista prod "+JSON.stringify(this.listaProd))
      for(let itemsDelCarrito of this.carrito){
         if(this.listaProd[itemProd].idProd == itemsDelCarrito.idProd){
          this.listaProd[itemProd].existenciaProd -= itemsDelCarrito.cantidad
          console.log("Id Producto Todos: "+ this.listaProd[itemProd].idProd);
          console.log("Id Producto Carrito: "+ itemsDelCarrito.idProd);
          console.log("Cantidad que se Compra "+itemsDelCarrito.cantidad);
          console.log("Cantidad que se Queda "+ this.listaProd[itemProd].existenciaProd);
          console.log("Datos del Producto Comprado "+ JSON.stringify( this.listaProd[itemProd]));
          console.log("Datos de todos los Productos "+JSON.stringify(this.listaProd));
        }
      }
    }
    this.vaciarCarrito();
    let nuevosDatos = this.listaProd;

    firebase.database().ref('/productos/').set(nuevosDatos)//Actualiza la BD en Firebase
  }

  //Funcion que vacia los productos del carrito
   public vaciarCarrito(){
     this.servicio.carritoLista = [];//Vacia la variable array en el servicio
     this.servicio.subtotalLista = [];//Vacia la variable array en el servicio
     this.carrito = [];//Vaciar la variable array del TS del carrito
     this.total = 0;//Poner valor cero a variable total
    this.router.navigate(['catalogo']);//Navegar a la vista catalogo
   }

borrar(id:number){
  this.carrito.splice(id, 1);
}




}
