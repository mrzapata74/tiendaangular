import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CatalogoComponent } from './componentes/catalogo/catalogo.component';
import { LoginComponent } from './componentes/login/login.component';
import { CarritoComponent } from './componentes/carrito/carrito.component';
import { ContactoComponent } from './componentes/contacto/contacto.component';
import { DetproductoComponent } from './componentes/detproducto/detproducto.component';
import { HomeComponent } from './componentes/home/home.component';
import { RegistroComponent } from './componentes/registro/registro.component';
import { EditprodComponent } from './componentes/editprod/editprod.component';
import { SidebarComponent } from './componentes/sidebar/sidebar.component';
import { identifierModuleUrl } from '@angular/compiler';

const routes: Routes = [
  {path:'', component: LoginComponent},

  {path: 'catalogo', component: CatalogoComponent},
  {path: 'carrito', component: CarritoComponent},
  {path: 'admin', component: HomeComponent},
  {path: 'producto', component: ContactoComponent},
  {path: 'editProd', component: EditprodComponent},
  {path: 'home', component: HomeComponent},
  {path: 'registro', component: RegistroComponent},
 /*  Se configura el path detalleProductos, con el parámetro :identifierModuleUrl,
  para que reciba el id del producto que viene al hacer click en el botón "Ver Más"
  del componente Catalogo.componente.html. */
  {path: 'detalleProducto/:id', component: DetproductoComponent},
  {path:'**', redirectTo: 'home'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
