import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetproductoComponent } from './detproducto.component';

describe('DetproductoComponent', () => {
  let component: DetproductoComponent;
  let fixture: ComponentFixture<DetproductoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetproductoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetproductoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
